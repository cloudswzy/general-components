package com.cloud.tool.util;

import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author WangZY
 * @classname MapUtil
 * @date 2022/9/23 15:23
 * @description
 */
public class MapUtil {
    /**
     * 将map切成段
     *
     * @param splitMap 被切段的map
     * @param splitNum 每段的大小
     * @param <k>      map的key类型
     * @param <v>      map的value类型 如果是自定义类型，则必须实现equals和hashCode方法
     * @return
     */
    public static <k, v> List<Map<k, v>> mapSplit(Map<k, v> splitMap, int splitNum) {
        if (splitMap == null || splitNum <= 0) {
            List<Map<k, v>> list = new ArrayList<>();
            list.add(splitMap);
            return list;
        }
        Set<k> keySet = splitMap.keySet();
        Iterator<k> iterator = keySet.iterator();
        int i = 1;
        List<Map<k, v>> total = new ArrayList<>();
        Map<k, v> tem = new HashMap<>();
        while (iterator.hasNext()) {
            k next = iterator.next();
            tem.put(next, splitMap.get(next));
            if (i == splitNum) {
                total.add(tem);
                tem = new HashMap<>();
                i = 0;
            }
            i++;
        }
        if (!CollectionUtils.isEmpty(tem)) {
            total.add(tem);
        }
        return total;
    }
}