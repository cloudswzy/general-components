package com.cloud.tool.exception;


/**
 * @Author WangZY
 * @Date 2021/6/18 15:59
 * @Description 工具箱异常
 **/
public class ToolException extends AbstractException {
    public ToolException(String errMsg) {
        super("通用组件异常:" + errMsg);
    }
}
