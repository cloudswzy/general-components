package com.cloud.tool.exception;

import lombok.Data;

/**
 * @Classname AbstractException
 * @Date 2022/4/13 17:05
 * @Author WangZY
 * @Description
 */
@Data
public abstract class AbstractException extends RuntimeException{
    private String errCode;
    private String errMsg;

    public AbstractException(String errCode,String errMsg){
        super(errMsg);
        this.errCode = errCode;
        this.errMsg = errMsg;
    }
    public AbstractException(String errMsg){
        super(errMsg);
        this.errCode = "10001";
        this.errMsg = errMsg;
    }
}
