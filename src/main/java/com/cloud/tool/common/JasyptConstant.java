package com.cloud.tool.common;

/**
 * @Author WangZY
 * @Date 2021/9/14 14:37
 * @Description 加密解密
 **/
public class JasyptConstant {
    public static final String DECRYPT = "decrypt";
    public static final String ENCRYPT = "encrypt";
}
