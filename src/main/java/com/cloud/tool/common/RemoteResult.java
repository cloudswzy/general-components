package com.cloud.tool.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @Classname RemoteResult
 * @Date 2022/4/13 18:30
 * @Author WangZY
 * @Description
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RemoteResult<T> {
    /**
     * 消息
     */
    private String msg;
    /**
     * 状态码
     */
    private String status;
    /**
     * 返回结果
     */
    private T data;

    public RemoteResult() {
    }

    public RemoteResult(String code, String msg) {
        this.status = code;
        this.msg = msg;
    }

    public RemoteResult(String code, String msg, T t) {
        this.status = code;
        this.msg = msg;
        this.data = t;
    }

    public RemoteResult(T data) {
        this.status = "200";
        this.msg = "操作成功";
        this.data = data;
    }
}
