package com.cloud.tool.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author WangZY
 * @Date 2022/2/21 15:37
 * @Description 限流器
 **/
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IdempotencyCheck {
    /**
     * 单次幂等性校验有效时间，默认60秒内不能重复提交，单位为秒
     */
    int checkTime() default 60;

    /**
     * 幂等性校验Key，
     * 默认为应用名(spring.application.name):当前方法名:当前登录人ID(没有SSO就是null):入参的md5值
     * 如果这里写了值就会替换入参和当前登录人--->应用名:当前方法名:checkKey
     */
    String checkKey() default "";
}
