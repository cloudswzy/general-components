package com.cloud.tool.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author WangZY
 * @Date 2022/2/21 15:37
 * @Description 限流器
 **/
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LimitMethod {
    /**
     * 限制次数
     */
    int limit() default 5;
}
