package com.cloud.tool.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Classname EncryptMethod
 * @Date 2021/8/3 14:44
 * @Author WangZY
 * @Description 加密方法
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface JasyptMethod {
    /**
     * 加密enc 解密dec
     */
    String type();
}
