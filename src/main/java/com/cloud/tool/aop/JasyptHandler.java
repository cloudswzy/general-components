package com.cloud.tool.aop;

import com.cloud.tool.annotation.JasyptField;
import com.cloud.tool.annotation.JasyptMethod;
import com.cloud.tool.common.JasyptConstant;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Collection;

/**
 * @Classname EncryptHandler
 * @Date 2021/8/3 14:34
 * @Author WangZY
 * @Description 数据加密切面
 */
@Aspect
@Component
@Slf4j
public class JasyptHandler {

    private final StringEncryptor stringEncryptor;

    public JasyptHandler(StringEncryptor stringEncryptor) {
        this.stringEncryptor = stringEncryptor;
    }

    @Pointcut("@annotation(com.cloud.tool.annotation.JasyptMethod)")
    public void pointCut() {
    }

    @Around("pointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        JasyptMethod jasyptMethod = methodSignature.getMethod().getAnnotation(JasyptMethod.class);
        String type = jasyptMethod.type();
        Object[] objects = joinPoint.getArgs();
        if (JasyptConstant.ENCRYPT.equalsIgnoreCase(type)) {
            try {
                if (objects.length != 0) {
                    for (int i = 0; i < objects.length; i++) {
                        if (objects[i] instanceof String) {
                            objects[i] = stringEncryptor.encrypt(String.valueOf(objects[i]));
                        } else {
                            processFields(objects[i], JasyptConstant.ENCRYPT);
                        }
                    }
                }
            } catch (Exception e) {
                log.error("TOOLBOX 数据加密失败", e);
            }
        }
        Object obj = joinPoint.proceed(objects);
        if (JasyptConstant.DECRYPT.equalsIgnoreCase(type) && obj != null) {
            if (obj instanceof String) {
                obj = stringEncryptor.decrypt(String.valueOf(obj));
            } else {
                processFields(obj, JasyptConstant.DECRYPT);
            }
        }
        return obj;
    }

    private void processFields(Object obj, String type) {
        Class<?> objClass = obj.getClass();
        // 避免递归检查基本类型、字符串类型或Object类
        if (objClass.isPrimitive() || objClass.equals(String.class) || objClass.equals(Object.class)) {
            return;
        }
        Field[] fields = objClass.getDeclaredFields();
        for (Field field : fields) {
            boolean hasSecureField = field.isAnnotationPresent(JasyptField.class);
            if (hasSecureField) {
                try {
                    //关闭JDK安全检查，提高反射速度
                    field.setAccessible(true);
                    Object fieldValue = field.get(obj);
                    // 如果字段是复杂类型，则递归处理
                    if (Collection.class.isAssignableFrom(field.getType())) {
                        Collection<?> collection = (Collection<?>) fieldValue;
                        if (collection != null) {
                            for (Object item : collection) {
                                if (item != null && !item.getClass().isPrimitive() && !item.getClass().equals(String.class)) {
                                    processFields(item, type); // 递归处理集合中的每个元素
                                }
                            }
                        }
                    } else if (!field.getType().isPrimitive() && !field.getType().equals(String.class)) {
                        field.setAccessible(true); // 设置可访问性以便读取对象
                        if (fieldValue != null) {
                            processFields(fieldValue, type); // 递归处理
                        }
                    } else {
                        String realValue = String.valueOf(fieldValue);
                        String value;
                        if (JasyptConstant.DECRYPT.equals(type)) {
                            value = stringEncryptor.decrypt(realValue);
                        } else {
                            value = stringEncryptor.encrypt(realValue);
                        }
                        field.set(obj, value);
                    }
                } catch (Exception e) {
                    log.error("TOOLBOX 反射修改对象属性失败", e);
                }
            }
        }
    }
}