package com.cloud.tool.log.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author WangZY
 * @Date 2021/12/6 18:58
 * @Description 切面排除该日志
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface IgnoreLog {
    /**
     * 是否传输日志到ES展示
     */
    boolean isEsCollect() default false;
}
