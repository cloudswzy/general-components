package com.cloud.tool.log.annotation;

import java.lang.annotation.*;

/**
 * @Classname EncryptMethod
 * @Date 2021/8/3 14:44
 * @Author WangZY
 * @Description TODO
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface LogCollector {
    /**
     * 是否记录请求参数
     */
    boolean isRecordRequest() default false;

    /**
     * 是否记录返回参数
     */
    boolean isRecordResponse() default false;

    /**
     * 是否记录主机IP
     */
    boolean isRecordIpAddr() default false;

    /**
     * 是否记录主机IP归属地
     */
    boolean isRecordRegion() default false;

    /**
     * 日志类型-可随意输入，或是选择constants.LogConstants
     */
    String logType();

    /**
     * 模块
     */
    String module();
}
