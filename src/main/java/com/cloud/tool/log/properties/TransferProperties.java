package com.cloud.tool.log.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "tool.log")
@Data
public class TransferProperties {
    /**
     * 系统中文名称
     */
    private String system;
    /**
     * kafka地址，可使用默认日志Kafka地址
     */
    private String kafkaServers = "xxxx:xxx";
    /**
     * 指定消费者组ID
     */
    private String consumerGroupId;
}