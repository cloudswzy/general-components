package com.cloud.tool.log.aop;

import com.alibaba.fastjson.JSON;
import com.cloud.tool.log.annotation.IgnoreLog;
import com.cloud.tool.log.annotation.LogCollector;
import com.cloud.tool.log.properties.TransferProperties;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @Classname EncryptHandler
 * @Date 2021/8/3 14:34
 * @Author WangZY
 * @Description 数据加密切面
 */
@Aspect
@Component
@EnableConfigurationProperties(value = {TransferProperties.class})
@Slf4j
public class LogTransferHandler {
    public static final String UAT_DEFAULT_TOPIC = "uat-request";
    public static final String PRO_DEFAULT_TOPIC = "pro-request";
    @Autowired
    private TransferProperties transferProperties;
    @Qualifier("performanceKafkaTemplate")
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    /**
     * 带有注解@LogCollector和controller层组成切点
     * TODO
     */
    @Pointcut("@annotation(com.cloud.tool.log.annotation.LogCollector)||execution(* *.修改包名..*.controller..*.*(..))")
    public void pointCut() {
    }

    @Resource
    private ConfigurableEnvironment config;


    @Around("pointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        //proceed方法用于启动目标方法执行，并能获取返回值信息
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert attributes != null;
        HttpServletRequest request = attributes.getRequest();
        Instant start = Instant.now();
        //TODO 获取当前用户信息
        String userId = "获取当前";
        String userName = "获取当前";
        String methodName = methodSignature.getName();
        //log4j2日志信息录入
        if (StringUtils.isEmpty(MDC.get("user_id")) && !StringUtils.isEmpty(userId) && !"null".equalsIgnoreCase(userId)) {
            MDC.put("user_id", userId);
        }
        if (StringUtils.isEmpty(MDC.get("user_name")) && !StringUtils.isEmpty(userName)
                && !"null".equalsIgnoreCase(userName)) {
            MDC.put("user_name", userName);
        }
        if (StringUtils.isEmpty(MDC.get("real_method"))) {
            MDC.put("real_method", "." + methodName);
        }
        boolean isHealthCheck = "healthcheck".equalsIgnoreCase(methodName) || "health".equalsIgnoreCase(methodName);
        String paramStr = getRequestParams(joinPoint);
        boolean isEsCollect = false;

        boolean annotationPresent = methodSignature.getMethod().isAnnotationPresent(IgnoreLog.class);
        if (!annotationPresent && !isHealthCheck) {
            log.info("请求参数:{}", paramStr);
        }
        Object result = joinPoint.proceed();
        Instant end = Instant.now();
        if (!annotationPresent && !isHealthCheck) {
            log.info("该方法执行返回:{},执行耗费:{}ms", JSON.toJSONString(result), ChronoUnit.MILLIS.between(start, end));
        }
        if (annotationPresent && !isHealthCheck) {
            IgnoreLog ignoreLog = methodSignature.getMethod().getAnnotation(IgnoreLog.class);
            isEsCollect = ignoreLog.isEsCollect();
        }
        if (methodSignature.getMethod().isAnnotationPresent(LogCollector.class)) {
            isEsCollect = true;
        }
        //通过log4j2直接传递日志，该处通过AOP收集稍显鸡肋，不过暂时保留，可改造为行为日志收集
        if (isEsCollect) {
            CompletableFuture.runAsync(() -> {
                String topic = "";
                if ("pro".equals(config.getProperty("env"))) {
                    topic = PRO_DEFAULT_TOPIC;
                } else {
                    topic = UAT_DEFAULT_TOPIC;
                }
                //TODO 自定义传输信息
                String msg = "";
                ListenableFuture<SendResult<String, String>> sendListener = kafkaTemplate.send(topic, msg);
                sendListener.addCallback(success -> {
                    log.info("日志传递成功,方法={},操作人={}", "xxx", "xxx");
                }, err -> {
                    log.error("日志传递失败,方法={},操作人={}", "xxx", "xxx");
                });
            }).handle((res, e) -> {
                if (e != null) {
                    log.error("日志传递出现异常", e);
                }
                return res;
            });
        }
        return result;
    }

    /***
     * @Author WangZY
     * @Date 2020/4/16 18:56
     * @Description 获取入参
     */
    private String getRequestParams(ProceedingJoinPoint proceedingJoinPoint) {
        Map<String, Object> requestParams = new HashMap<>(16);
        //参数名
        String[] paramNames = ((MethodSignature) proceedingJoinPoint.getSignature()).getParameterNames();
        //参数值
        Object[] paramValues = proceedingJoinPoint.getArgs();
        for (int i = 0; i < paramNames.length; i++) {
            Object value = paramValues[i];
            //如果是文件对象
            if (value instanceof MultipartFile) {
                MultipartFile file = (MultipartFile) value;
                //获取文件名
                value = file.getOriginalFilename();
                requestParams.put(paramNames[i], value);
            } else if (value instanceof HttpServletRequest) {
                requestParams.put(paramNames[i], "参数类型为HttpServletRequest");
            } else if (value instanceof HttpServletResponse) {
                requestParams.put(paramNames[i], "参数类型为HttpServletResponse");
            } else {
                requestParams.put(paramNames[i], value);
            }
        }
        return JSON.toJSONString(requestParams);
    }
}
