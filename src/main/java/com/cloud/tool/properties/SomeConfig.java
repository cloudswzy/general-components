package com.cloud.tool.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 不同的环境启用不同的配置文件
 */
@Configuration
@PropertySource("classpath:tool-${spring.profiles.active}.properties")
@Data
public class SomeConfig {
    @Value("${here}")
    private String here;
}