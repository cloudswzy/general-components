package com.cloud.tool.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * @Classname ToolAutoConfiguration
 * @Date 2021/12/7 10:50
 * @Author WangZY
 * @Description 统一注册BEAN
 */
@ComponentScan(basePackages = "com.cloud.tool")
public class ToolAutoConfiguration {
}
