package com.cloud.tool.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @Classname RegistryBeanFactoryConfig
 * @Date 2021/12/6 18:39
 * @Author WangZY
 * @Description 删除三方框架中Bean
 */
@Component
public class BusinessBeanConfig implements BeanDefinitionRegistryPostProcessor {
    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        //这里仅仅做一个案例分享，意思是如果存在这个bean，就删除掉
        if (registry.containsBeanDefinition("globalDefaultExceptionHandler")) {
            registry.removeBeanDefinition("globalDefaultExceptionHandler");
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }
}