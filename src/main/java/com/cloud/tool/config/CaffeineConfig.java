package com.cloud.tool.config;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

/**
 * @author WangZY
 * @date 2022/5/31 16:37
 * @description Caffeine缓存配置
 */
@Configuration
public class CaffeineConfig {
    /**
     * 本地缓存更建议自己去配置，尽量不要公用
     */
    @Bean
    public Cache<String, Object> commonCaffeine() {
        return Caffeine.newBuilder()
                //初始大小
                .initialCapacity(1000)
                //PS：expireAfterWrite和expireAfterAccess同时存在时，以expireAfterWrite为准。
                //最后一次写操作后经过指定时间过期
//                .expireAfterWrite(Duration.ofMinutes(30))
                //最后一次读或写操作后经过指定时间过期
                .expireAfterAccess(Duration.ofHours(16))
                // 最大数量，默认基于缓存内的元素个数进行驱逐
                .maximumSize(10000)
                //打开数据收集功能  hitRate(): 查询缓存的命中率 evictionCount(): 被驱逐的缓存数量 averageLoadPenalty(): 新值被载入的平均耗时
//                .recordStats()
                .build();
//// 查找一个缓存元素， 没有查找到的时候返回null
//        Object obj = cache.getIfPresent(key);
//// 查找缓存，如果缓存不存在则生成缓存元素,  如果无法生成则返回null
//        obj = cache.get(key, k -> createExpensiveGraph(key));
//// 添加或者更新一个缓存元素
//        cache.put(key, graph);
//// 移除一个缓存元素
//        cache.invalidate(key);
//// 批量失效key
//        cache.invalidateAll(keys)
//// 失效所有的key
//        cache.invalidateAll()
    }
}
