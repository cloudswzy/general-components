package com.cloud.tool.config;

import com.cloud.tool.properties.SomeConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @Classname ToolApplicationContextInitializer
 * @Date 2021/8/3 17:32
 * @Author WangZY
 * @Description 启动后检查参数
 */
@Component
public class ToolApplicationContextInitializer implements ApplicationRunner {
    @Autowired
    private ConfigurableEnvironment config;
    @Autowired
    private SomeConfig someConfig;

    @Override
    public void run(ApplicationArguments args) {
        //这个给一个扩展的地方，我们可以在SDK里面定义好参数直接使用
        System.out.println("---------------当前环境是" + someConfig.getHere());
        String jasyptPassword = config.getProperty("jasypt.encryptor.password");
        if (StringUtils.isEmpty(jasyptPassword)) {
            System.out.println("提示，当前项目没有配置jasypt.encryptor.password，不能使用加密解密注解");
        }
    }
}
