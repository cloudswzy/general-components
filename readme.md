## 创建自己的spring boot starter
首先最重要的第一步就是如何创建自己的spring boot starter，这个很简单啊，直接用模板就行了

### 初始化组件包结构
创建一个spring boot项目，用https://start.spring.io/或者Idea创建都可以，删除启动类，把包精简成上面这个样子
### 创建spring.factories文件
resources包下手动创建一个META-INFO文件夹，并且在包下创建一个spring.factories文件，文件内容写，注意空格(使用Idea会有提示)

```
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
  com.cloud.tool.config.ToolAutoConfiguration
```

spring.factories的工作原理类似于Java原生的SPI机制，在此基础上进行了优化，可以一个文件写多个接口，想深入了解的可以搜一下@EnableAutoConfiguration这个注解的工作原理。
### 创建统一注册类

```
import org.springframework.context.annotation.ComponentScan;
//统一注册BEAN
@ComponentScan(basePackages = "com.cloud.tool")
public class ToolAutoConfiguration {
}
```

ToolAutoConfiguration这个类主要是方便做bean的注册，@ComponentScan这个注解会扫描并加载属性basePackages指定的包路径下所有bean，就不用在spring.factories文件逐个写了，只用写这个类就行了。@SpringBootApplication启动类注解也使用了@ComponentScan。

### pom文件配置
配置如下，着重关注下groupId和artifactId，这是组件在maven仓库里的坐标。finalname指定最终打包后的名称，install命令将包下载到本地，就是你在maven的setting.xml里配置的本地仓库，deploy命令会推送你的包到远程仓库(例如公司的私服)

```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.cloud.tool</groupId>
    <artifactId>general-components</artifactId>
    <version>1.0.0</version>
    <name>general-components</name>
    <description>通用组件</description>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
            <version>2.3.12.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-aop</artifactId>
            <version>2.3.12.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-validation</artifactId>
            <version>2.3.12.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>2.3.12.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.22</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.redisson</groupId>
            <artifactId>redisson</artifactId>
            <version>3.17.0</version>
        </dependency>
        <dependency>
            <groupId>com.github.ulisesbocchio</groupId>
            <artifactId>jasypt-spring-boot-starter</artifactId>
            <version>3.0.4</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>2.2.2</version>
        </dependency>
    </dependencies>
    <build>
        <finalName>toolbox</finalName>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>2.3.12.RELEASE</version>
            </plugin>
        </plugins>
    </build>
    <!-- 用来支持项目发布到私服中,用来配合deploy插件的使用 -->
    <distributionManagement>
        <repository>
            <id>xxx</id>
            <name>xxx</name>
            <url>http://xxx</url>
        </repository>
    </distributionManagement>
</project>
```


以上四步完成后就建立起了一个spring boot starter的架子，接下来就可以大展身手了(突然想到了一部番名：无职转生，到了异世界就拿出真本事，哈哈)
## 加密模块
加密用到了以下五个类，两个依赖和几项配置
JasyptField--提供注解JasyptField用于对象属性以及方法参数
JasyptMethod--提供注解JasyptMethod用于注解在方法上
JasyptHandler--由切面方式实现，使用时请务必注意切面使用禁忌
JasyptConstant--常量
JasyptMybatisHandler--mybatis处理扩展


```
<dependency>
    <groupId>com.github.ulisesbocchio</groupId>
    <artifactId>jasypt-spring-boot-starter</artifactId>
    <version>3.0.4</version>
</dependency>
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.2.2</version>
</dependency>
```



```
# 加密盐值，如果报空指针找不到该行，查看代码是否有config类
jasypt.encryptor.password=wzy
# 加密前缀及后缀默认ENC( )，例如密文SCM(s0kO6zE9NteZrzUbpzeCE9PinvI)
jasypt.encryptor.property.prefix=SCM(
jasypt.encryptor.property.suffix=)
# 加密算法设置3.0.0以后
jasypt.encryptor.algorithm=PBEWithMD5AndDES
jasypt.encryptor.iv-generator-classname=org.jasypt.iv.NoIvGenerator
```


加密工具类我没有自己写，原因是jasypt实在是太好用了，https://github.com/ulisesbocchio/jasypt-spring-boot 我用的spring boot版本的，里面readme比较详细，http://www.jasypt.org/原版官网，不过我觉得普通用用，看看博客就行了，上手更快。
加密的话，我一开始是只写了一种，切面JasyptHandler做接口出入参的加密解密，后来同事反馈说咱们用mybatis直接扩展不就行了，好家伙说得妙啊，mybatis果然有这样的扩展类TypeHandler，然后根据这个写了个JasyptMybatisHandler
### 切面加密
切面的切入点是@JasyptMethod，目前测试过的有String，obj，List<obj>，如有没支持的可以自行扩充。逻辑也很简单，获取入参后，根据类型判断，如果是对象的话，会去反射获取所有字段，判断字段上有没有注解@JasyptField，如果有的话，进行加密解密
### Mybatis扩展
这个是我们在项目里常用的，使用方式很简单，指定typeHandle为自定义的类就行了，基于mybatis提供的扩展窗口，对String类型的字段在填入和取出时分别进行加密和解密
使用时，如果是mybatis-plus，务必在表映射实体类上增加注解@TableName(autoResultMap = true)

使用时须在对应实体类字段上加 typeHandler = JasyptMybatisHandler.class

## Redis+Lua工具包
使用Lua脚本，原因：
1.减少网络开销。可以将多个请求通过脚本的形式一次发送，减少网络时延。使用lua脚本执行以上操作时，比redis普通操作快80%左右
2.原子操作。Redis会将整个脚本作为一个整体执行，中间不会被其他请求插入。因此在脚本运行过程中无需担心会出现竞态条件，无需使用事务。
3.复用。客户端发送的脚本会永久存在redis中，这样其他客户端可以复用这一脚本，而不需要使用代码完成相同的逻辑。
### redis单号生成器
单号按照keyPrefix+yyyyMMdd+4位流水号的格式生成
redis获取当前keyPrefix对应的key，如果没有则返回1，如果存在，判断是否大于9999，如果大于返回错误，如果小于就将value+1，并且设置过期时间直到今天结束。
### redis漏斗算法限流器
基于Lua+Aop，切点是@LimitMethod，注解参数是同时运行次数，使用场景是前后端的接口
@Around运行实际方法前进行限流(使用次数自增)，@AfterRunning后返还使用次数
## 配置类
### BusinessBeanConfig
这个类用于帮助你排除bean用的，比如我就拿来排除掉公司框架里的全局异常监听
### MyRedissonConfig
Redisson的配置类，主要是启动参数的配置以及编码的配置，这里全局设置的String，用这个主要是因为想要保证Redis数据的可读性
### ThreadPoolConfig
线程池的配置，这里简单的进行了CPU型和IO型的配置，可以通过ToolProperties类在application配置类写简单参数，更具体的就自定义吧。这个线程池用的Spring包装后的ThreadPoolTaskExecutor，注意JDK原版的是ThreadPoolExecutor，Spring Boot项目的话，建议还是用包装后的
### ToolApplicationContextInitializer
启动时一次性运行类，我一般用来做配置文件校验。因为有的配置给不了默认值，只能强制让同事写，比如系统中文名
### ValidatorConfig
@Valid配置快速失败，类似于ArrayList的fail-fast机制，有错了就抛，不会校验后面的数据
## 美化Controller层
美化后效果，需要统一返回值并且配置全局异常监听

### 统一返回值
有一个类似于RemoteResult的类，包含状态码，消息，返回值，如果你有更多的内容需要输出那就扩展这个类
### 全局异常监听
简易版的会用到三个类
异常类ToolException，标准异常，错误码，错误信息。
抽象异常类AbstractException，这个类的主要作用是提供一个异常的架子，方便扩展，如果没啥需求，可以不用这个，只提供普通异常类就行
全局异常监听类ToolExceptionHandler，在这个类里面去监听不同的错误，根据不同的错误来进行对应的处理

感谢看到最后，分享下我的博客地址(里面有我的碎碎念，补充了markdown缺失的图片)
https://juejin.cn/post/7087875951010250759/

微信号：skyqiyunhun
如果想讨论问题，请注明  掘金+[问题类型]